FROM ruby:2.4.0-alpine

WORKDIR /app
EXPOSE 5000
CMD ["sh", "-c", "while true; do ruby ./server.rb; done"]

COPY . /app
