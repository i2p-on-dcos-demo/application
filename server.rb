require 'webrick'

server = WEBrick::HTTPServer.new :Port => 5000

server.mount_proc '/' do |request, response|
  response.body =
    <<~HTML
    <html>
      <head>
        <meta charset="utf-8">
        <title>GitLab I2P on DC/OS demo</title>
      </head>
      <body>
        <h1>GitLab I2P on DC/OS demo</h1>
        <p>Hello world!</p>
      </body>
    </html>
    HTML
end

server.start
