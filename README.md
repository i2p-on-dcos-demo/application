# GitLab I2P on DC/OS demo

Simple ruby application created for a Idea To Production based
on DC/OS demo.

## Author

Tomasz Maczukin, 2017, GitLab

## License

MIT
